package ru.botyanov.library.entity;

import org.apache.log4j.Logger;
import ru.botyanov.library.action.LibraryAction;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Exchanger;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class User implements Runnable {
    public static final Logger LOG = Logger.getLogger(User.class);
    private static final int MAX_READING_TIME = 150;
    private static Exchanger<Book> exchanger = new Exchanger<>();

    private int id;
    private ArrayList<Book> booksOnHand;
    private ArrayList<Book> booksTaken;
    private Library library;

    public User(int id, ArrayList<Book> booksOnHand, ArrayList<Book> booksTaken, Library library) {
        this.id = id;
        this.booksOnHand = booksOnHand;
        this.booksTaken = booksTaken;
        this.library = library;
    }

    @Override
    public void run() {
        booksOnHand.add(LibraryAction.takeBook(library));
        booksTaken.add(LibraryAction.takeBook(library));

        try {
            TimeUnit.MILLISECONDS.sleep(new Random().nextInt(MAX_READING_TIME));

            LOG.debug(toString() + " " + booksOnHand + " before exchange");
            int i = new Random().nextInt(getBooksOnHand().size());
            ArrayList<Book> exchangerTemp = getBooksOnHand();
            exchangerTemp.add(exchanger.exchange(getBooksOnHand().get(i), 1600, TimeUnit.MILLISECONDS));
            exchangerTemp.remove(i);
            setBooksOnHand(exchangerTemp);
            LOG.debug(toString() + " " + booksOnHand + " after exchenge");
        } catch (InterruptedException | TimeoutException e) {
            LOG.error(e+":Some user didn't find anybody to exchange");
        }

        LibraryAction.putBook(library, booksOnHand.get(0));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<Book> getBooksOnHand() {
        return (ArrayList<Book>)booksOnHand.clone();
    }

    public void setBooksOnHand(ArrayList<Book> booksOnHand) {
        this.booksOnHand = booksOnHand;
    }

    public ArrayList<Book> getBooksTaken() {
        return (ArrayList<Book>)booksTaken.clone();
    }

    public void setBooksTaken(ArrayList<Book> booksTaken) {
        this.booksTaken = booksTaken;
    }

    public Library getLibrary() {
        return library;
    }

    public void setLibrary(Library library) {
        this.library = library;
    }
}