package ru.botyanov.library.entity;

import ru.botyanov.library.creator.BookCreator;
import ru.botyanov.library.creator.ReaderCreator;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

public class Library {
    private static AtomicBoolean bool = new AtomicBoolean(false);
    private static Library instance;
    private static ReentrantLock lock = new ReentrantLock();
    private ArrayList<User> users;
    private ConcurrentHashMap<Book,Integer> books;

    private Library(){
    }

    public static Library getInstance(){
        if(!bool.get()) {
            try {
                lock.lock();
                if (instance == null) {
                    init();
                    bool.set(true);
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    public ArrayList<User> getUsers() {
        return (ArrayList<User>)users.clone();
    }

    public void setUsers(ArrayList<User> users) {
        this.users = users;
    }

    public ConcurrentHashMap<Book, Integer> getBooks() {
        return books;
    }

    public void setBooks(ConcurrentHashMap<Book, Integer> books) {
        this.books = books;
    }

    private static void init(){
        instance = new Library();
        ArrayList<User> users = new ReaderCreator().create();
        ConcurrentHashMap<Book,Integer> books = new BookCreator().create();
        instance.setBooks(books);
        instance.setUsers(users);
    }

}