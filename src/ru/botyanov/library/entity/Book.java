package ru.botyanov.library.entity;

import ru.botyanov.library.exception.BookIllegalArgumentException;

public class Book implements Cloneable{
    private int id;
    private String name;
    private String author;

    public Book(int id,String name, String author) {
        this.id = id;
        this.name = name;
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws BookIllegalArgumentException {
        if (name != null && !name.isEmpty()) {
                this.name = name;
            } else {
                throw new BookIllegalArgumentException();
            }
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) throws BookIllegalArgumentException {
        if (author != null && !author.isEmpty()) {
                this.author = author;
            } else {
                throw new BookIllegalArgumentException();
            }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) throws BookIllegalArgumentException{
        if (id > 0) {
            this.id = id;
        } else {
            throw new BookIllegalArgumentException();
        }
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", author='" + author + '\'' +
                '}';
    }

}