package ru.botyanov.library.exception;

public class BookIllegalArgumentException extends Exception {
    public BookIllegalArgumentException() {
        super();
    }

    public BookIllegalArgumentException(String message) {
        super(message);
    }

    public BookIllegalArgumentException(String message, Throwable cause) {
        super(message, cause);
    }

    public BookIllegalArgumentException(Throwable cause) {
        super(cause);
    }

    protected BookIllegalArgumentException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
