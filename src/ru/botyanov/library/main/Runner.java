package ru.botyanov.library.main;

import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import ru.botyanov.library.entity.Library;
import ru.botyanov.library.entity.User;

public class Runner {
    public static final String LOG_PATH = "config/log4j.xml";

    static {
        new DOMConfigurator().doConfigure(LOG_PATH, LogManager.getLoggerRepository());
    }

    public static void main(String[] args) {
        for (User user : Library.getInstance().getUsers()) {
            new Thread(user).start();
        }

    }
}