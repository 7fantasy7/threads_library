package ru.botyanov.library.creator;

import ru.botyanov.library.entity.Book;

import java.util.concurrent.ConcurrentHashMap;

public class BookCreator {

    public ConcurrentHashMap<Book,Integer> create(){
        ConcurrentHashMap<Book,Integer> books = new ConcurrentHashMap<>();

        books.put(new Book(1, "Java programming language", "James Gosling"), 1);
        books.put(new Book(2, "Effective Java", "James Bloch"), 1);
        books.put(new Book(3, "Head First Servlets and JSP", "O'Reilly"), 1);
        books.put(new Book(4, "Head First Desing Patterns", "O'Reilly"), 2);
        books.put(new Book(5, "Refactoring to patterns", "Kerievsky"), 1);
        books.put(new Book(6, "Anti-patterns", "William J Brown"), 1);
        books.put(new Book(7, "Thinking in Java", "Bruce Eckel"), 2);
        books.put(new Book(8, "Java Concurency in Practice", "Goetz B."), 1);
        books.put(new Book(9, "Core java", "Cay S. Horstmann"), 3);
        books.put(new Book(10, "JSTL in Action", "Shawn Bayern"), 1);

        return books;
    }
}