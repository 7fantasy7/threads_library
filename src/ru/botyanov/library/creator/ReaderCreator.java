package ru.botyanov.library.creator;

import ru.botyanov.library.entity.Library;
import ru.botyanov.library.entity.User;

import java.util.ArrayList;

public class ReaderCreator {

    public ArrayList<User> create(){
        ArrayList<User> users = new ArrayList<>();

        users.add(new User(1, new ArrayList<>(), new ArrayList<>(), Library.getInstance()));
        users.add(new User(2, new ArrayList<>(), new ArrayList<>(), Library.getInstance()));
        users.add(new User(3, new ArrayList<>(), new ArrayList<>(), Library.getInstance()));
        users.add(new User(4, new ArrayList<>(), new ArrayList<>(), Library.getInstance()));
        users.add(new User(5, new ArrayList<>(), new ArrayList<>(), Library.getInstance()));
        users.add(new User(6, new ArrayList<>(), new ArrayList<>(), Library.getInstance()));
        users.add(new User(7, new ArrayList<>(), new ArrayList<>(), Library.getInstance()));

        return users;
    }
}