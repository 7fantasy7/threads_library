package ru.botyanov.library.action;

import org.apache.log4j.Logger;
import ru.botyanov.library.entity.Book;
import ru.botyanov.library.entity.Library;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LibraryAction {
    public static final Logger LOG = Logger.getLogger(LibraryAction.class);
    private static Lock lock = new ReentrantLock();

    public static void putBook(Library library, Book book) {
            library.getBooks().put(book, library.getBooks().get(book)+1);
    }

    public static Book takeBook(Library library, Book book) {
        Book foundBook = null;
        try {
            lock.lock();
            ConcurrentHashMap<Book, Integer> books = library.getBooks();
            if(library.getBooks().get(book) > 0) {
                foundBook = book;
                books.put(foundBook, library.getBooks().get(book)-1);
            }

        } finally {
            lock.unlock();
        }
        return foundBook;
    }

    public static Book takeBook(Library library) {
        Book book = null;
        try {
            lock.lock();
            LOG.info(Thread.currentThread().getName() + " is taking book now");
            ConcurrentHashMap<Book, Integer> books = library.getBooks();
            for (Book b : books.keySet()) {
                if (books.get(b) > 0) {
                    book = b;
                    books.put(b, library.getBooks().get(book) - 1);
                    break;
                }
            }
        } finally {
            lock.unlock();
        }
        return book;
    }

}